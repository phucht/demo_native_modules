package  com.demo_native_modules;

import android.app.Activity;
import android.content.Intent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import  android.hardware.Camera;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.TextView;
import  android.widget.Toast;

import  com.facebook.react.bridge.NativeModule;
import  com.facebook.react.bridge.ReactApplicationContext;
import  com.facebook.react.bridge.ReactContext;
import  com.facebook.react.bridge.ReactContextBaseJavaModule;
import  com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import org.w3c.dom.Text;

import java.io.InputStream;

import javax.annotation.Nullable;

public  class  MyModule extends  ReactContextBaseJavaModule{
     public  MyModule(ReactApplicationContext reacContext) {
         super(reacContext);

     }

     private   ReactApplicationContext reactContext;
     private  Camera camera = null;
     private Uri fileUri;
      private static  final  int REQUEST_IMAGE_CAPTURE =1;
     Bitmap bitmap;


     @Override
      public  String getName(){
         return  "MyModule";
     }


    @ReactMethod
     public  void openCamera(){
             Activity activity= getCurrentActivity();
             Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
             activity.startActivityForResult(takePictureIntent,REQUEST_IMAGE_CAPTURE);
      }

}


