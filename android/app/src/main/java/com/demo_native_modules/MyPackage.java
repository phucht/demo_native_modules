package  com.demo_native_modules;

import android.graphics.Camera;
import android.view.View;
import android.widget.TextView;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.views.image.ReactImageManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MyPackage implements ReactPackage{


    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
       List<NativeModule> modules =new ArrayList<>();
        modules.add(new MyModule(reactContext));
        return  modules;
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return  Collections.emptyList();
    }
}
